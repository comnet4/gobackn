import socket
import time
import sys

class GBNStruct:
    def __init__(self, seq_no, ack_no, data_size, data):
        self.seq_no = seq_no
        self.ack_no = ack_no
        self.data_size = data_size
        self.data = data
        self.is_acknowledged = False
        self.last_sent_time = None

    def serialize(self):
        seq_no_bytes = self.seq_no.to_bytes(4, byteorder='big')
        ack_no_bytes = self.ack_no.to_bytes(4, byteorder='big')
        data_size_bytes = self.data_size.to_bytes(4, byteorder='big')
        serialized_data = seq_no_bytes + ack_no_bytes + data_size_bytes + self.data
        return serialized_data

    @staticmethod
    def deserialize(data):
        seq_no = int.from_bytes(data[0:4], byteorder='big')
        ack_no = int.from_bytes(data[4:8], byteorder='big')
        data_size = int.from_bytes(data[8:12], byteorder='big')
        packet_data = data[12:]
        return GBNStruct(seq_no, ack_no, data_size, packet_data)
    
PORT = 12349;
SERVER = '127.0.0.1'
RECEIVER_ADDR = (SERVER, PORT)
CHUCK_SIZE = 100
LOSS_RATE = 1.0
WINDOW_SIZE = 4
TRYS = 10
TIMEOUT = 2

def openFile(filename):
    packets = [];
    next_seq_num = 0
    with open(filename, 'rb') as file:
        while True:
            data = file.read(CHUCK_SIZE)
            if not data:
                break
            packet = GBNStruct(next_seq_num, next_seq_num, len(data), data)
            packet.last_sent_time = time.time()
            packets.append(packet)
            next_seq_num += 1
    print(f"Total Packet: {len(packets)} \n")
    return packets, next_seq_num;

def sender(sock):
    
    base = 0
    trys = TRYS
    packets, next_seq_num = openFile('filetosend.txt')

    start_time = time.time()
    
    start_time = time.time()
    while base < len(packets):
        for i in range(base, min(base + WINDOW_SIZE, len(packets))):
            if i == base or (i < next_seq_num and not packets[i].is_acknowledged):
                if time.time() - packets[i].last_sent_time >= TIMEOUT:
                    print(f"Sending packet {i}")
                    sock.sendto(packets[i].serialize(), (SERVER, PORT))
                    packets[i].last_sent_time = time.time()
                    
                    if i == WINDOW_SIZE+base-1 or (len(packets)-base < WINDOW_SIZE) and  i==len(packets)-1:
                        trys-=1
                        print(f"Trys more {trys} \n")
                    if trys == 0:
                        print("No Response: Interrupted system call")
                        return                                                  
        try:
            data, _ = sock.recvfrom(1024)
            ack_packet = GBNStruct.deserialize(data)
            if ack_packet is not None and hasattr(ack_packet, 'ack_no'): 
                ack_no = ack_packet.ack_no
                if packets[ack_no].is_acknowledged is False: 
                    print(f"\nReceived ACK for packet {ack_no} {packets[ack_no].data}")
                    packets[ack_no].is_acknowledged = True
                    trys = 10
                    if ack_no == base:
                        while base < len(packets) and packets[base].is_acknowledged:
                            base += 1
        except socket.timeout:
            pass
    
    end_time = time.time()
    total_time = end_time - start_time
    throughput = len(packets) / total_time
    print(f"\nFile transfer completed in {total_time:.2f} seconds.")
    print(f"Throughput: {throughput:.2f} packets per second")
    sock.close()

if __name__ == '__main__':
    
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    CHUCK_SIZE = int(sys.argv[3])
    WINDOW_SIZE = int(sys.argv[4])
    
    print(f"INPUT SERVER: {SERVER}: {PORT}. CHUCK_SIZE: {CHUCK_SIZE}. WINDOW_SIZE: {WINDOW_SIZE}.")
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0.1)
    sender(sock);
    

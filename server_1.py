import socket
import sys
import random
import time

PORT = 12349;
RECEIVER_ADDR = ('127.0.0.1', PORT)
CHUCK_SIZE = 100
LOSS_RATE = 1.0
def simulate_packet_loss(loss_rate):
    return random.uniform(0.1, 0.9) >= LOSS_RATE

def receive(socket, data, Rn):
    while True:
        DATA, ADDR = socket.recvfrom(CHUCK_SIZE)
        seq_no = int.from_bytes(DATA[0:4], byteorder='big')
        if Rn == seq_no and simulate_packet_loss(LOSS_RATE):
            socket.sendto(DATA, ADDR)  
            print(f"Received {len(data)} bytes from {ADDR}") 
            data += DATA[12:] 
            print("Result: ",data) 
            Rn+=1
        else:
            #Implicit
            print(f"Packet Loss, sequence number {seq_no} from {ADDR}") 

if __name__ == '__main__':
    Rn = 0
    PORT = int(sys.argv[1])
    CHUCK_SIZE = int(sys.argv[2])
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', PORT))
    print(f"Server is listening {RECEIVER_ADDR} with chunk size {CHUCK_SIZE} and loss rate {LOSS_RATE}")

    received_data = b""  
    receive(server_socket, received_data, Rn)
